import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './layout/header/header.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { PatientsComponent } from './patients/patients.component';
import { ConsultationsComponent } from './consultations/consultations.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path:'', // localhost:4200/
    redirectTo:'home', // localhost:4200/login
    pathMatch: 'full'
  },
  {
    path: 'home', 
    component: HeaderComponent,
    children:[
      {path: '', component: HomeComponent}
    ]
  },
  {
    path: 'doctors', 
    component: HeaderComponent,
    children: [
      {path: '', component: DoctorsComponent}
    ]
  },
  {
    path: 'patients', 
    component: HeaderComponent,
    children: [
      {path: '', component: PatientsComponent}
    ]
  },
  {
    path: 'consultations', 
    component: HeaderComponent,
    children: [
      {path: '', component: ConsultationsComponent}
    ]
  },
  {
    path: 'schedules', 
    component: HeaderComponent,
    children: [
      {path: '', component: SchedulesComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
