import { Injectable } from '@angular/core';
import { GenericService } from '../app.service';
import { Utils } from '../shared/utils/utils';
import { Observable } from 'rxjs';
import { ResponseModel } from '../shared/response-model';
import { HttpClient } from '@angular/common/http';
import { Consultation } from '../shared/models/consultation.model';

@Injectable({
    providedIn: 'root'
})
export class ConsultationsService {
    private apiUrl: string;
    constructor(private http: HttpClient, 
        private service: GenericService) {
        this.apiUrl = Utils.UrlApi();
    }
    getAll(): Observable<any> {
        return this.service.getAll('/consultations');
    }

    getById(id: number): Observable<ResponseModel> {
        return this.service.getById(id, '/consultations');
    }

    create(object: Consultation): Observable<ResponseModel> {
        return this.service.create(object, '/consultations');
    }

    update(object: Consultation): Observable<ResponseModel> {
        return this.service.update(object, '/consultations');
    }

    delete(id: number): Observable<ResponseModel> {
        return this.service.delete(id, '/consultations');
    }

    getByDoctorId(doctorId:number): Observable<any> {
        return this.http.get<any>(`${this.apiUrl}/consultations/${doctorId}`);
    }
}