import { Component, OnInit } from '@angular/core';
import { Doctor } from '../shared/models/doctor.model';
import { Patient } from '../shared/models/patient.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConsultationsService } from './consultation.service';
import { DoctorsService } from '../doctors/doctors.service';
import { PatientsService } from '../patients/patients.service';
import { Consultation } from '../shared/models/consultation.model';

@Component({
  selector: 'app-consultations',
  templateUrl: './consultations.component.html',
  styleUrls: ['./consultations.component.css']
})
export class ConsultationsComponent implements OnInit {
  feedbackMessage: string = '';
  patients: Patient[];
  doctors: Doctor[];

  form: FormGroup;
  constructor(
    private formBuilder: FormBuilder, 
    private consultationsService: ConsultationsService,
    private doctorsService: DoctorsService,
    private patientsService: PatientsService,) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      doctorId: [null, [Validators.required]],
      patientId: [null, [Validators.required]],
      scheduledTime: [null, [Validators.required]]
    });

    this.loadDoctorsList();
    this.loadPatientsList();
  }

  loadDoctorsList(){
    this.doctorsService.getAll().subscribe(
      response => {
        this.doctors = response;
      }
    )
  }

  loadPatientsList(){
    this.patientsService.getAll().subscribe(
      response => {
        this.patients = response;
      }
    )
  }

  RegisterConsultation(){
    let consultation = new Consultation();
    consultation.UserId = 1;
    consultation.DoctorId = this.form.get('doctorId').value;
    consultation.PatientId = this.form.get('patientId').value;
    consultation.ScheduledTime = this.form.get('scheduledTime').value;
    this.doctorsService.getById(consultation.DoctorId).subscribe(
      response => {
        consultation.Specialty = response.data.specialty.name;
      }
    );
    
    this.consultationsService.create(consultation).subscribe(
      response => {
        if(response.status === 1){
          this.feedbackMessage = response.message;
        }
      }
    )
  }

  OnSubmit(){
    this.RegisterConsultation();
  }

  get f() {return this.form.controls}
}
