import { Injectable } from '@angular/core';
import { GenericService } from '../app.service';
import { Utils } from '../shared/utils/utils';
import { Observable } from 'rxjs';
import { ResponseModel } from '../shared/response-model';
import { Specialty } from '../shared/models/specialty.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class SpecialtiesService {
    private apiUrl: string;
    constructor(private http: HttpClient, private service: GenericService) {
        this.apiUrl = Utils.UrlApi();
    }
    getAll(): Observable<any> {
        return this.service.getAll('/specialties');
    }

    getById(id: number): Observable<ResponseModel> {
        return this.service.getById(id, '/specialties');
    }

    create(object: Specialty): Observable<ResponseModel> {
        return this.service.create(object, '/specialties');
    }

    update(object: Specialty): Observable<ResponseModel> {
        return this.service.update(object, '/specialties');
    }

    delete(id: number): Observable<ResponseModel> {
        return this.service.delete(id, '/specialties');
    }
}