import { Component, OnInit } from '@angular/core';
import { DoctorSchedule } from '../shared/models/doctor-schedule.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SchedulesService } from './schedule.service';
import { Doctor } from '../shared/models/doctor.model';
import { DoctorsService } from '../doctors/doctors.service';

@Component({
  selector: 'app-schedules',
  templateUrl: './schedules.component.html',
  styleUrls: ['./schedules.component.css']
})
export class SchedulesComponent implements OnInit {

  schedules: DoctorSchedule[];
  doctors: Doctor[];

  form: FormGroup;
  constructor(
    private formBuilder: FormBuilder, 
    private schedulesService: SchedulesService,
    private doctorsService: DoctorsService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      doctorId: [null, [Validators.required]],
      startTime: [null, [Validators.required]],
      endTime: [null, [Validators.required]]
    });

    this.loadDoctorsList();
  }

  loadDoctorsList(){
    this.doctorsService.getAll().subscribe(
      response => {
        this.doctors = response;
        console.log(response);
      }
    )
  }

  loadDoctorSchedules(doctorId:number){
    this.schedulesService.getByDoctorId(doctorId).subscribe(
      response => {
        this.schedules = response;
        console.log(response);
      }
    )
  }

  OnDoctorSelected(selectedValue){
    this.loadDoctorSchedules(selectedValue);
  }

  RegisterSchedule(){
    let schedule = new DoctorSchedule();
    schedule.DoctorId = this.form.get('doctorId').value;
    schedule.StartTime = this.form.get('startTime').value;
    schedule.EndTime = this.form.get('endTime').value;

    console.log(schedule);
    this.schedulesService.create(schedule).subscribe(
      response => {
        if(response.status === 1){
          this.form.get('startTime').reset();
          this.form.get('endTime').reset();

          let doctorId: number = this.form.get('doctorId').value;
          this.loadDoctorSchedules(doctorId);
        }
      }
    )
  }
  OnSubmit(){
    this.RegisterSchedule();
  }
  get f () { return this.form.controls}
}
