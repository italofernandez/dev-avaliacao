import { Injectable } from '@angular/core';
import { GenericService } from '../app.service';
import { Utils } from '../shared/utils/utils';
import { Observable } from 'rxjs';
import { ResponseModel } from '../shared/response-model';
import { HttpClient } from '@angular/common/http';
import { DoctorSchedule } from '../shared/models/doctor-schedule.model';

@Injectable({
    providedIn: 'root'
})
export class SchedulesService {
    private apiUrl: string;
    constructor(private http: HttpClient, 
        private service: GenericService) {
        this.apiUrl = Utils.UrlApi();
    }
    getAll(): Observable<any> {
        return this.service.getAll('/doctorSchedule');
    }

    getById(id: number): Observable<ResponseModel> {
        return this.service.getById(id, '/doctorSchedule');
    }

    create(object: DoctorSchedule): Observable<ResponseModel> {
        return this.service.create(object, '/doctorSchedule');
    }

    update(object: DoctorSchedule): Observable<ResponseModel> {
        return this.service.update(object, '/doctorSchedule');
    }

    delete(id: number): Observable<ResponseModel> {
        return this.service.delete(id, '/doctorSchedule');
    }

    getByDoctorId(doctorId:number): Observable<any> {
        return this.http.get<any>(`${this.apiUrl}/doctorSchedule/${doctorId}`);
    }
}