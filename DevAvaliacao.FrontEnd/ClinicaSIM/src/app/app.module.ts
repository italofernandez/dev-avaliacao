import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { PatientsComponent } from './patients/patients.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { ConsultationsComponent } from './consultations/consultations.component';
import { LayoutModule } from './layout/layout.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { DoctorsService } from './doctors/doctors.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { GenericService } from './app.service';
import { SpecialtiesComponent } from './specialties/specialties.component';
import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';
import { ConsultationsService } from './consultations/consultation.service';
registerLocaleData(ptBr);
@NgModule({
  declarations: [
    AppComponent,
    DoctorsComponent,
    PatientsComponent,
    SchedulesComponent,
    ConsultationsComponent,
    HomeComponent,
    SpecialtiesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,ReactiveFormsModule
  ],
  providers: [
    FormBuilder,
    HttpClient,
    GenericService,
    DoctorsService,
    ConsultationsService,
    [{provide: LOCALE_ID, useValue: 'pt'}]
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
