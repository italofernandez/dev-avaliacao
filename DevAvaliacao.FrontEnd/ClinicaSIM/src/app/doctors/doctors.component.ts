import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DoctorsService } from './doctors.service';
import { SpecialtiesService } from '../specialties/specialties.service';
import { Specialty } from '../shared/models/specialty.model';
import { Doctor } from '../shared/models/doctor.model';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class DoctorsComponent implements OnInit {
  showSpecialties = true;
  specialties: Specialty[];
  doctors: Doctor[];
  specialtyFeedbackMessage: string = '';
  
  form: FormGroup;
  formSpecialty: FormGroup;
  constructor(
    private formBuilder: FormBuilder, 
    private doctorService: DoctorsService,
    private specialtyService: SpecialtiesService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(3)]],
      crm: [null, [Validators.required]],
      specialtyId: [null, [Validators.required]],
      isGeneralist: [false]
    });

    this.formSpecialty = this.formBuilder.group({
      specialtyName: [null, [Validators.required]]
    });

    this.loadDoctorList();

    this.loadSpecialties();
  }

  onChangeIsGeneralist(value: boolean){
    if(value)
    {
      this.showSpecialties = false;
      this.form.get('specialtyId').setValue(1);
    }
    else
      this.showSpecialties = true;
  }

  loadSpecialties(){
    this.specialtyService.getAll().subscribe(
      response => {
        this.specialties = response;
      }
    );
  }

  registerDoctor(){
    let doctor = new Doctor();
    doctor.Name = this.form.get('name').value;
    doctor.CRM = this.form.get('crm').value;
    doctor.IsGeneralist = this.form.get('isGeneralist').value;
    doctor.SpecialtyId = this.form.get('specialtyId').value;

    this.doctorService.create(doctor).subscribe(
      response => {
        if(response.status === 1){
          this.form.reset();
          this.loadDoctorList();
        }
      }
    );
  }

  loadDoctorList(){
    this.doctorService.getAll().subscribe(
      response => {
        this.doctors = response;
      }
    );
  }

  OnSubmit(){
    this.registerDoctor();
  }

  registerSpecialty(){
    let specialty = new Specialty();
    specialty.Name = this.formSpecialty.get('specialtyName').value;

    this.specialtyService.create(specialty).subscribe(
      response => {
        if(response.status === 1){
          this.loadSpecialties();
          this.specialtyFeedbackMessage = 'A especialidade foi adicionada';
        }
      }
    )
  }
}
