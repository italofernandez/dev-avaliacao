import { Injectable } from '@angular/core';
import { GenericService } from '../app.service';
import { Utils } from '../shared/utils/utils';
import { Observable } from 'rxjs';
import { ResponseModel } from '../shared/response-model';
import { Doctor } from '../shared/models/doctor.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DoctorsService {
    private apiUrl: string;
    constructor(private http: HttpClient, private service: GenericService) {
        this.apiUrl = Utils.UrlApi();
    }
    getAll(): Observable<any> {
        return this.service.getAll('/doctors');
    }

    getById(id: number): Observable<ResponseModel> {
        return this.service.getById(id, '/doctors');
    }

    create(object: Doctor): Observable<ResponseModel> {
        return this.service.create(object, '/doctors');
    }

    update(object: Doctor): Observable<ResponseModel> {
        return this.service.update(object, '/doctors');
    }

    delete(id: number): Observable<ResponseModel> {
        return this.service.delete(id, '/doctors');
    }

    cnhExist(crm: string): Observable<boolean> {
        return this.http.get<boolean>(`${this.apiUrl}/doctors/crmExist/${crm}`, {
            headers: this.service.composeHeaders()
        });
    }
}