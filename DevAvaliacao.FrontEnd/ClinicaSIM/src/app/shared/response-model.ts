export class ResponseModel{
    status: number;
    message: string;
    location: string;
    innerException: string;
    notifications: Notification[];
    data: any;
    totalRecords: number;
}

class Notification {
    property: string;
    message: string;
}