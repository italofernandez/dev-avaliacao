export class DoctorSchedule
{
    Id: number;
    DoctorId: number;
    StartTime: Date;
    EndTime: Date;
}