export class Consultation
{
    Id: number;
    PatientId: number;
    DoctorId: number;
    UserId: number;
    Specialty: string;
    ScheduledTime: Date;
}