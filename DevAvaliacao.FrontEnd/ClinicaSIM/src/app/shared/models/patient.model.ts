export class Patient
{
    Id: number;
    Name: string;
    Phone: string;
    Email: string;
    CPF: string;
}