export class Doctor
{
    Id: number;
    Name: string;
    CRM: string;
    SpecialtyId: number;
    IsGeneralist: boolean;
}