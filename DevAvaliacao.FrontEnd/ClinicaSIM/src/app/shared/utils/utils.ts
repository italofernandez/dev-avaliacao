export class Utils {
    public static UrlApi(): string {
        const url: string = window.location.hostname;
        return 'http://localhost:5000/api';
    }

    public static dataAtualFormatada(): string {
        var data = new Date(),
            dia = data.getDate().toString().padStart(2, '0'),
            mes = (data.getMonth() + 1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
            ano = data.getFullYear() + 1;
        return ano + "-" + mes + "-" + dia;
    }

    public static formatDate(date: Date): string {
        var ano = date.toString().substring(0, 4);
        var mes = date.toString().substring(5, 7);
        var dia = date.toString().substring(8, 10);

        return dia + '/' + mes + '/' + ano;
    }
}