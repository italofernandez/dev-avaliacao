import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Utils } from './shared/utils/utils';
import { Observable } from 'rxjs';
import { ResponseModel } from './shared/response-model';

@Injectable()
export class GenericService {
    url: string;
    headers: HttpHeaders;

    constructor(private http: HttpClient) {
        this.url = Utils.UrlApi();
    }

    public composeHeaders()
    {
        const headers = new HttpHeaders();
        return headers;
    }
    
    getAll(route:string):Observable<any>{
        return this.http.get<ResponseModel>(`${this.url}${route}`, { headers: this.composeHeaders() });
    }
    
    getWithFiltersAndPagination(filter: any, pageSize: number, pageNumber: number, route: string): Observable<ResponseModel> {
        return this.http.get<ResponseModel>(`${this.url}${route}/${pageSize}/${pageNumber}`, { params: filter, headers: this.composeHeaders() });
    }

    getById(id: number, route: string) : Observable<ResponseModel> { 
      return this.http.get<ResponseModel>(`${this.url}${route}/${id}`, { headers: this.composeHeaders() });
    }

    create(object: any, route: string): Observable<ResponseModel> {
        return this.http.post<ResponseModel>(`${this.url}${route}`, object, { headers: this.composeHeaders() });
    }
    
    update(object: any, route: string): Observable<ResponseModel> {
        return this.http.put<ResponseModel>(`${this.url}${route}`, object, { headers: this.composeHeaders() });
    }
    
    delete(id: number, route: string): Observable<ResponseModel> {
        return this.http.delete<ResponseModel>(`${this.url}${route}/${id}`);
    }
}