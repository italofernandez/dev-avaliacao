import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PatientsService } from './patients.service';
import { Patient } from '../shared/models/patient.model';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {
  patients: Patient[];

  form: FormGroup;
  constructor(
    private formBuilder: FormBuilder, 
    private patientService: PatientsService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(3)]],
      phone: [null, [Validators.required, Validators.minLength(8)]],
      email: [null, [Validators.required]],
      cpf: [null, [Validators.required, Validators.minLength(11)]]
    });
    this.loadPatientsList();
  }

  loadPatientsList(){
    this.patientService.getAll().subscribe(
      response => {
        this.patients = response;
      }
    )
  }

  registerPatient(){
    let patient = new Patient();
    patient.CPF = this.form.get('cpf').value;
    patient.Name = this.form.get('name').value;
    patient.Email = this.form.get('email').value;
    patient.Phone = this.form.get('phone').value;

    this.patientService.create(patient).subscribe(
      response => {
        if(response.status === 1){
          this.form.reset();
          this.loadPatientsList();
        }
      }
    )
  }

  OnSubmit(){
    this.registerPatient();
  }

  get f() { return this.form.controls; }
}
