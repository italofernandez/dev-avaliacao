import { Injectable } from '@angular/core';
import { GenericService } from '../app.service';
import { Utils } from '../shared/utils/utils';
import { Observable } from 'rxjs';
import { ResponseModel } from '../shared/response-model';
import { Patient } from '../shared/models/patient.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class PatientsService {
    private apiUrl: string;
    constructor(private http: HttpClient, private service: GenericService) {
        this.apiUrl = Utils.UrlApi();
    }
    getAll(): Observable<any> {
        return this.service.getAll('/patients');
    }

    getById(id: number): Observable<ResponseModel> {
        return this.service.getById(id, '/patients');
    }

    create(object: Patient): Observable<ResponseModel> {
        return this.service.create(object, '/patients');
    }

    update(object: Patient): Observable<ResponseModel> {
        return this.service.update(object, '/patients');
    }

    delete(id: number): Observable<ResponseModel> {
        return this.service.delete(id, '/patients');
    }
}